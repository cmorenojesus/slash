import React from 'react';
import { Input, Row, Col, InputGroup, InputGroupText, InputGroupAddon, } from 'reactstrap';
import Contenedor from './Contenedor.jsx';
import '../css/home.css';
import Icon from '@mdi/react';
import { mdiHeart } from '@mdi/js';




export default class Home extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            datos: '',
            buscar: '',
            activado: false,
            likes: 0,
            color: "#76D7C4",
            color2: "white",
            color3: "#76D7C4",

        }

        this.fetchData = this.fetchData.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.calculaLikes = this.calculaLikes.bind(this);
        this.cambiarColor = this.cambiarColor.bind(this);
        this.cambiarColor2 = this.cambiarColor2.bind(this);
    }

    cambiarColor() {
        this.setState({
            color: "white",
            color2: "#909497",
            color3: "#909497",

        });
    }

    cambiarColor2() {
        this.setState({
            color: "#76D7C4",
            color2: "white",
            color3: "#76D7C4"

        });
    }

    fetchData() {
        fetch(`https://itunes.apple.com/search?term=${this.state.buscar}&entity=song`)
            .then(data => data.json())
            .then(data => {
                this.setState({ datos: data.results })
                console.log("¡Datos conseguidos!");
            })
            .catch(error => console.log('Algo ha ido mal...', error))
    }

    handleChange(event) {
        const target = event.target;
        const value = target.value;
        this.setState({
            buscar: value
        });

    }

    calculaLikes(valor) {
        this.setState({
            likes: this.state.likes + valor
        });
    }

    render() {

        let lista = [];

        if (this.state.datos.length) {
            let i = 0;
            lista = this.state.datos.map(el => <Contenedor calcula={this.calculaLikes} key={i++} elementos={el} activado={this.state.activado} />);
        }

        return (


            <div id="body">

                <Row id="header" className="container-fluid" >

                    <Col xs="6" sm="6" md={{ size: 6, offset: 3 }}>

                        <InputGroup>
                            <Input placeholder="Busca un artista..." style={{ outline: "none", boxShadow: "none", border: "1px solid #ccc", borderRight: "none" }} onChange={this.handleChange} value={this.state.buscar} />
                            <InputGroupAddon addonType="append">
                                <InputGroupText onMouseOver={this.cambiarColor} onMouseOut={this.cambiarColor2} id="lupa" onClick={this.fetchData} style={{ backgroundColor: this.state.color2, border: "1px solid", borderColor: this.state.color3 }}><svg style={{ width: "24px", height: "24px" }} viewBox="0 0 24 24">
                                    <path fill={this.state.color} d="M9.5,3A6.5,6.5 0 0,1 16,9.5C16,11.11 15.41,12.59 14.44,13.73L14.71,14H15.5L20.5,19L19,20.5L14,15.5V14.71L13.73,14.44C12.59,15.41 11.11,16 9.5,16A6.5,6.5 0 0,1 3,9.5A6.5,6.5 0 0,1 9.5,3M9.5,5C7,5 5,7 5,9.5C5,12 7,14 9.5,14C12,14 14,12 14,9.5C14,7 12,5 9.5,5Z" />
                                </svg></InputGroupText>

                            </InputGroupAddon>

                        </InputGroup>
                    </Col>

                    <Col xm="2">
                        <Icon className="contador" color="white" path={mdiHeart} size={1.2}></Icon>
                        <div className="likes">{this.state.likes}</div>

                    </Col>


                </Row>

                <Row className="row justify-content-center" id="britney">

                    {lista}
                </Row>

            </div >


        );



    }





}